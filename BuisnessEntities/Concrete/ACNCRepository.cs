﻿using BuisnessEntities.Entities;
using BuisnessEntities.Utilities;
using DataAccessLayer.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BuisnessEntities.Concrete
{
    public class ACNCRepository:IDisposable
    {
        private ACNCEntities Context;
        public ACNCRepository()
        {
            this.Context = new ACNCEntities();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public PagingResult<ACNCModel> GetListOfACNCDetails(FilterCriteriaModel filterCriteria,bool allowPaging = true)
        {

            ObjectParameter output = new ObjectParameter("TotalResults", typeof(int));
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GetACNCDetails(filterCriteria.CareForId, filterCriteria.HelpForId, filterCriteria.FacilityId, filterCriteria.PageIndex,filterCriteria.PageSize, filterCriteria.SearchColumn,output).ToList();

            ConfigurationRepository.SuspendExecutionStrategy = false;

            var dataCount = list.Select(x => new ACNCModel
            {
                CharityName = x.CharityName,
                Address = x.Address,
                Town = x.Town_City,
                charityWebsite = x.Charity_Website,
                Size = x.Charity_Size
                
            }).ToList();
            return new PagingResult<ACNCModel>()
            {
                Status = ActionStatus.Successfull,
                DataList = dataCount,
                TotalResults = Convert.ToInt32(output.Value)
            };
        }

        public IEnumerable<SelectListItem> GetCareFor()
        {

            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.MostImportants.AsEnumerable().Select(x=> new SelectListItem
            {
                Text=x.CareFor,
                Value=x.Id.ToString(),
                Selected=false
            });

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return list;
        }

        public List<SelectListItem> GetHelpFor(int CareForId)
        {

            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.HelpForCares.ToList().Where(x=>x.MostImportantId==CareForId).Select(x => new SelectListItem
            {
                Text = x.HelpCategory,
                Value = x.Id.ToString(),
                Selected = false
            }).ToList();

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return list;
        }

        public List<SelectListItem> GetFacility(int HelpForId)
        {

            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.TypesOfFacilities.ToList().Where(x => x.HelpCareID == HelpForId).Select(x => new SelectListItem
            {
                Text = x.Facility,
                Value = x.Id.ToString(),
                Selected = false
            }).ToList();

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return list;
        }

        public void GetListOfOldPeople(out string CountOldPeopleList, out string InYearList)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.getOldAgeDetail().Select(x => new Graph
            {
              CountOfPeople=x.CountOfOldPeople.ToString(),
              Year = x.year.ToString()
            }).ToList();

            var countData = list.Select(x => x.CountOfPeople).ToList();
            CountOldPeopleList = string.Join(",", countData);

            var yearData = list.Select(x => x.Year).ToList();
            InYearList = string.Join(",", yearData);

            ConfigurationRepository.SuspendExecutionStrategy = false;
            
        }

        public PagingResult<ACNCModel> SearchACNCDetails(FilterCriteriaModel filterCriteria)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;
            ObjectParameter output = new ObjectParameter("TotalResults", typeof(int));
            var list = Context.SearchForCharity(filterCriteria.SearchColumn,filterCriteria.PageIndex,filterCriteria.PageSize, output).ToList();

            ConfigurationRepository.SuspendExecutionStrategy = false;

            var dataCount = list.Select(x => new ACNCModel
            {
                CharityName = x.CharityName,
                Address = x.Address,
                Town = x.Town_City,
                charityWebsite = x.Charity_Website,
                Size = x.Charity_Size

            }).ToList();
            return new PagingResult<ACNCModel>()
            {
                Status = ActionStatus.Successfull,
                DataList = dataCount,
                TotalResults = Convert.ToInt32(output.Value)
            };
        }

    }
}
