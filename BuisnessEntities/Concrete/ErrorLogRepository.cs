﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using BuisnessEntities.Abstract;
using BuisnessEntities.Utilities;
using DataAccessLayer.Model.DataModel;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace BuisnessEntities.Concrete
{
    public class ErrorLogRepository: IErrorLogRepository, IDisposable
    {
        private ACNCEntities Context;
        public ErrorLogRepository()
        {
            this.Context = new ACNCEntities();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        string IErrorLogRepository.LogExceptionToDatabase(Exception ex, int userId)
        {
            ErrorLog obj_errorlog = null;
            String ex_text = String.Empty;
            String ex_message = ex.Message;
            try
            {
                Context = new ACNCEntities();
                obj_errorlog = new ErrorLog();
                obj_errorlog.Message = ex.Message;
                obj_errorlog.CompleteError = ex.ToString();
                obj_errorlog.LoggedInDetails = userId;
                obj_errorlog.CreatedOn = DateTime.Now;

                //=======================================================================

                ex_text = new JavaScriptSerializer().Serialize(obj_errorlog);
                Context.ErrorLogs.Add(obj_errorlog);
                Context.Entry(obj_errorlog).State = System.Data.Entity.EntityState.Added;
                Context.SaveChanges();
                return obj_errorlog.Id.ToString();

            }
            catch (DbUpdateException exe)
            {
                Utility.LogExceptionToFile(exe.Message.ToString(), userId.ToString());
                return "0";
            }
            catch (DbEntityValidationException exe)
            {
                Utility.LogExceptionToFile(exe.ToString(), userId.ToString());
                return "0";
            }
        }
        void IErrorLogRepository.LogExceptionToFile(string ex, int userId)
        {
            Utility.LogExceptionToFile(ex, userId.ToString());

            //System.IO.StreamWriter sw = null;
            //if (!File.Exists(HostingEnvironment.MapPath("~/ErrorLog.txt")))
            //    File.Create(HostingEnvironment.MapPath("~/ErrorLog.txt"));
            //sw = new StreamWriter(HostingEnvironment.MapPath("~/ErrorLog.txt"), true);
            //sw.WriteLine(ex_message);
            //sw.WriteLine("/");
            //sw.WriteLine(ex); sw.WriteLine(""); sw.WriteLine("");
            //sw.Close();
        }
    }
}
