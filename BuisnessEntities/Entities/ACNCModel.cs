﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Entities
{
    public class ACNCModel
    {
        public float ABN { get; set; }
        public string CharityName { get; set; }
        public string Address { get; set; }
        public string Town { get; set; }
        public string charityWebsite { get; set; }
        public string Size { get; set; }

    }

    public class Graph
    {
        public string  Year { get; set; }
        public string CountOfPeople { get; set; }
    }
}
