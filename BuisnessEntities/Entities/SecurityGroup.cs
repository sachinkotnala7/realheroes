﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BuisnessEntities.Entities
{
    public class SecurityGroup
    {
        
    }

    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public bool Deleted { get; set; }

        public int UserRoleId { get; set; }
    }

    public class RoleListModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int ModuleId { get; set; }
    }
    //public class ActionOutputBase
    //{
    //    public ActionStatus Status { get; set; }
    //    public String Message { get; set; }

    //}

    //public class ActionOutput<T> : ActionOutputBase
    //{
    //    public T Object { get; set; }
    //    public List<T> Results { get; set; }
    //}

    //public class ActionOutput : ActionOutputBase
    //{
    //    public long ID { get; set; }
    //}

    public enum ActionStatus
    {
        Successfull = 200,
        Error = 400,
        LoggedOut = 201,
        Unauthorized = 202
    }
}
