﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Utilities
{
    public class FilterCriteriaModel
    {
        public FilterCriteriaModel()
        {
            SortByList = new List<KeyValuePair>();
            SearchColumnList = new List<KeyValuePair>();
        }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
        public string SearchColumn { get; set; }

        [StringLength(50, ErrorMessage = "Search Keyword should be less than 50 characters.")]
        public string SerachCloumnValue { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }

        public List<KeyValuePair> SortByList { get; set; }
        public List<KeyValuePair> SearchColumnList { get; set; }

        public int CareForId { get; set; }
        public int HelpForId { get; set; }
        public int FacilityId { get; set; }
    }

    public class KeyValuePair
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class ListModel<T>
    {
        public ListModel()
        {
            PagingResult = new PagingResult<T>();
            FilterCriteriaModel = new FilterCriteriaModel();
        }
        public PagingResult<T> PagingResult;
        public FilterCriteriaModel FilterCriteriaModel;
    }
}
