﻿using MvcGlobalisationSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Charity
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();


            //routes.MapRoute
            // (
            // name: "CharityHome",
            // url: "Charity/Home/{ContextId}",
            // defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            // );

            const string defautlRouteUrl = "{controller}/{action}/{id}";

            RouteValueDictionary defaultRouteValueDictionary = new RouteValueDictionary(new
            {
                controller = "Home",
                action = "Index",
                id = UrlParameter.Optional
            });
            routes.Add("DefaultGlobalised", new GlobalisedRoute(defautlRouteUrl, defaultRouteValueDictionary));
            routes.Add("Default", new Route(defautlRouteUrl, defaultRouteValueDictionary, new MvcRouteHandler()));

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}
