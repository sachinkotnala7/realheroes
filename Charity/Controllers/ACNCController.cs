﻿using BuisnessEntities.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuisnessEntities.Concrete;
using DCCA.Controllers;

namespace Charity.Controllers
{
    public class ACNCController : BaseController
    {
       
        public ACNCController() : base()
        {
            
        }
        // GET: ACNC
        public ActionResult GetACNCDetails(FilterCriteriaModel criteria)
        {
            var result = new ACNCRepository().GetListOfACNCDetails(criteria);
            string view = RenderRazorViewToString("_LoadResults", result.DataList);
            result.Data = view;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCareFor()
        {
            var result = new ACNCRepository().GetCareFor().ToList();
            string view = RenderRazorViewToString("_CareFor", result);
            return Json(new ActionOutput<string>{
                Object = view,
                Status = BuisnessEntities.Entities.ActionStatus.Successfull
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHelpFor(int CareForId)
        {
            var result = new ACNCRepository().GetHelpFor(CareForId);
            string view = RenderRazorViewToString("_HelpForData", result);
            return Json(new ActionOutput<string>
            {
                Object = view,
                Status = BuisnessEntities.Entities.ActionStatus.Successfull
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFacilities(int HelpForId)
        {
            var result = new ACNCRepository().GetFacility(HelpForId);
            string view = RenderRazorViewToString("_Facilities", result);
            return Json(new ActionOutput<string>
            {
                Object = view,
                Status = BuisnessEntities.Entities.ActionStatus.Successfull
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Volunteer()
        {
            try
            {
                string CountOldPeopleList = string.Empty;
                string InYearList = string.Empty;

                new ACNCRepository().GetListOfOldPeople(out  CountOldPeopleList, out InYearList);
                ViewBag.PeopleCount_List = CountOldPeopleList.Trim();
                ViewBag.Year_List = InYearList.Trim();

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SearchForACNCDetails(FilterCriteriaModel filter)
        {
            var result = new ACNCRepository().SearchACNCDetails(filter);
            string view = RenderRazorViewToString("_LoadResults", result.DataList);
            result.Data = view;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}