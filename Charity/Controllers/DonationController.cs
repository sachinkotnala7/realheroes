﻿using BuisnessEntities.Concrete;
using BuisnessEntities.Entities;
using BuisnessEntities.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Charity.Controllers
{
    public class DonationController : BaseController
    {
        public DonationController() : base()
        {

        }
        // GET: Donation
        public ActionResult Donate()
        {
            FilterCriteriaModel searchModel = new FilterCriteriaModel();
            searchModel.PageSize = 10;
            ListModel<ACNCModel> model = new ListModel<ACNCModel>();
            model.PagingResult = new ACNCRepository().GetListOfACNCDetails(searchModel);
            model.FilterCriteriaModel = searchModel;
            return View(model);
        }
    }
}