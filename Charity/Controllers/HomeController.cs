﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuisnessEntities.Abstract;
using BuisnessEntities.Concrete;
using BuisnessEntities.Entities;
using Charity.Controllers;

namespace DCCA.Controllers
{
    public class HomeController : BaseController
    {
        private IErrorLogRepository ErrorLogRepository;
        public HomeController() : base(){
            this.ErrorLogRepository = new ErrorLogRepository();
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}