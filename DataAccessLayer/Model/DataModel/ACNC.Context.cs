﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer.Model.DataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ACNCEntities : DbContext
    {
        public ACNCEntities()
            : base("name=ACNCEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<ACNC_TABLE> ACNC_TABLE { get; set; }
        public virtual DbSet<HelpForCare> HelpForCares { get; set; }
        public virtual DbSet<MostImportant> MostImportants { get; set; }
        public virtual DbSet<TypesOfFacility> TypesOfFacilities { get; set; }
    
        public virtual ObjectResult<getOldAgeDetail_Result> getOldAgeDetail()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getOldAgeDetail_Result>("getOldAgeDetail");
        }
    
        public virtual ObjectResult<SearchForCharity_Result> SearchForCharity(string searchFeild, Nullable<int> offset, Nullable<int> size, ObjectParameter totalResults)
        {
            var searchFeildParameter = searchFeild != null ?
                new ObjectParameter("SearchFeild", searchFeild) :
                new ObjectParameter("SearchFeild", typeof(string));
    
            var offsetParameter = offset.HasValue ?
                new ObjectParameter("offset", offset) :
                new ObjectParameter("offset", typeof(int));
    
            var sizeParameter = size.HasValue ?
                new ObjectParameter("size", size) :
                new ObjectParameter("size", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SearchForCharity_Result>("SearchForCharity", searchFeildParameter, offsetParameter, sizeParameter, totalResults);
        }
    
        public virtual ObjectResult<GetACNCDetails_Result> GetACNCDetails(Nullable<int> mostImportantId, Nullable<int> helpForCareId, Nullable<int> typeOfFacilityId, Nullable<int> offset, Nullable<int> size, string searchFeild, ObjectParameter totalResults)
        {
            var mostImportantIdParameter = mostImportantId.HasValue ?
                new ObjectParameter("mostImportantId", mostImportantId) :
                new ObjectParameter("mostImportantId", typeof(int));
    
            var helpForCareIdParameter = helpForCareId.HasValue ?
                new ObjectParameter("helpForCareId", helpForCareId) :
                new ObjectParameter("helpForCareId", typeof(int));
    
            var typeOfFacilityIdParameter = typeOfFacilityId.HasValue ?
                new ObjectParameter("TypeOfFacilityId", typeOfFacilityId) :
                new ObjectParameter("TypeOfFacilityId", typeof(int));
    
            var offsetParameter = offset.HasValue ?
                new ObjectParameter("offset", offset) :
                new ObjectParameter("offset", typeof(int));
    
            var sizeParameter = size.HasValue ?
                new ObjectParameter("size", size) :
                new ObjectParameter("size", typeof(int));
    
            var searchFeildParameter = searchFeild != null ?
                new ObjectParameter("SearchFeild", searchFeild) :
                new ObjectParameter("SearchFeild", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetACNCDetails_Result>("GetACNCDetails", mostImportantIdParameter, helpForCareIdParameter, typeOfFacilityIdParameter, offsetParameter, sizeParameter, searchFeildParameter, totalResults);
        }
    }
}
